export const fetchData = async (url: string): Promise<any> => {
  try {
    const response = await fetch(url);
    return await response.json();
  } catch (e) {
    console.error(`An error occurred while fetching data : ${e}`);
  }
};
