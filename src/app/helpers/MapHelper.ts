import { LatLngExpression } from 'leaflet';

export const reverseLngLat = (coordinates: number[]): LatLngExpression => {
  const latitude: number = coordinates[1];
  const longitude: number = coordinates[0];

  return [latitude, longitude];
};
