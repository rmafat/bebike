import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { RouteService } from '../../services/route.service';
import { Subscription } from 'rxjs';
import * as Feather from 'feather-icons';
import { Instruction, InstructionSign } from '../../models/route';
import { RouteFormService } from '../../services/route-form.service';
import { MODES } from '../../models/modes';

@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.scss']
})
export class RoadmapComponent implements OnInit, AfterViewInit, OnDestroy {

  private routeSubscription: Subscription;
  private routeFormSubscription: Subscription;
  private departure: string;
  private arrival: string;
  private date: Date;
  private MODES = MODES;
  private mode: MODES;
  private time: number;
  private distance: number;
  private instructions: Instruction[];
  private Signs = InstructionSign;

  constructor(private routeService: RouteService, private routeFormService: RouteFormService) { }

  ngOnInit(): void {
    this.routeSubscription = this.routeService.routeSubject.subscribe(
      (route) => {
         if (route) {
           this.time = route.time;
           this.distance = route.distance;
           this.instructions = route.instructions;
        }
      },
      error => console.error(`An error occurred on route subscription : ${error}`)
    );

    this.routeService.emitRoute();

    this.routeFormSubscription = this.routeFormService.routeFormSubject.subscribe(
      (route) => {
        this.departure = route.departure.properties.name;
        this.arrival = route.arrival.properties.name;
        this.date = route.date.toDate();
        this.mode = route.mode;
      },
      error => console.error(`An error occured on route form subscription : ${error}`)
    );

    this.routeFormService.emitRouteForm();
  }

  ngAfterViewInit(): void {
    Feather.replace();
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
    this.routeFormSubscription.unsubscribe();
  }

  private formatTime = (time: number): string => (time < 10 ? `0${time}` : time.toString());

  getTime = (timeInMs: number): string => {
    const timeInSeconds = timeInMs / 1000;
    if (timeInSeconds < 60) {
      return `${Math.round(timeInSeconds)}s`;
    }

    const timeInMinutes = timeInSeconds / 60;
    if (timeInMinutes < 60) {
      return `${Math.round(timeInMinutes)}min`;
    }

    const timeInHours = timeInMinutes / 60;
    if (timeInHours < 24) {
      return `${Math.trunc(timeInHours)}h${this.formatTime(Math.round(timeInMinutes % 60))}`;
    }

    const timeInDays = timeInHours / 24;
    const hoursRest = timeInHours % 24;
    return `${Math.trunc(timeInDays)}j${this.formatTime(Math.trunc(hoursRest))}h${this.formatTime(Math.round((hoursRest * 60) % 60))}`;
  }

  getDistance = (distance: number): string => {
    if (distance < 1000) {
      return `${Math.round(distance)}m`;
    }

    return `${(distance / 1000).toFixed(2)}km`;
  }

  onEdit = () => this.routeService.hideResults();
}
