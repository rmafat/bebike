import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import * as Feather from 'feather-icons';
import { MODES } from '../../../models/modes';
import { AUTOCOMPLETE_ID } from '../autocomplete-address/autocomplete-address.component';
import { RouteFormService } from '../../../services/route-form.service';
import { MatRadioChange } from '@angular/material';
import { Subscription } from 'rxjs';
import RouteForm from '../../../models/routeForm';
import { RouteService } from '../../../services/route.service';
import routeForm from '../../../models/routeForm';

@Component({
  selector: 'app-route-form',
  templateUrl: './route-form.component.html',
  styleUrls: ['./route-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RouteFormComponent implements OnInit, OnDestroy {

  AUTOCOMPLETE_ID = AUTOCOMPLETE_ID;
  MODES = MODES;
  routeFormSubscription: Subscription;
  routeForm: routeForm;

  constructor(private routeFormService: RouteFormService, private routeService: RouteService) {
  }

  ngOnInit() {
    Feather.replace();

    this.routeFormSubscription = this.routeFormService.routeFormSubject.subscribe(
      (data: RouteForm) => {
        this.routeForm = data;
      },
      error => console.error(`An error occurred on route subscription : ${error}`),
    );

    this.routeFormService.emitRouteForm();
  }

  ngOnDestroy(): void {
    this.routeFormSubscription.unsubscribe();
  }

  onModeChange = (event: MatRadioChange) => this.routeFormService.setMode(event.value);
  onSwitchDirections = () => this.routeFormService.switchDirections();
  shouldDisableSubmit = () => !this.routeForm.departure || !this.routeForm.arrival;
  onSubmit = () => this.routeService.fetchRoute(this.routeForm);
}
