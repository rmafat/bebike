import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { RouteFormService } from '../../../services/route-form.service';
import { Subscription } from 'rxjs';
import RouteForm from '../../../models/routeForm';
import { Moment } from 'moment';
import { MatDatepickerInputEvent, MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DateTimeComponent implements OnInit, OnDestroy {

  routeSubscription: Subscription;
  now: Moment;
  date: Moment;
  hours: number;
  minutes: number;
  minDate: Date;
  allHours: number[];
  allMinutes: number[];

  constructor(private routeFormService: RouteFormService) {}

  ngOnInit() {
    this.now = moment();

    this.routeSubscription = this.routeFormService.routeFormSubject.subscribe(
      (route: RouteForm) => {
        this.date = route.date;
        this.hours = this.date.hours();
        this.minutes = this.date.minutes();
      },
      error => console.error(`An error occurred on route subscription : ${error}`),
    );

    this.routeFormService.emitRouteForm();

    this.allHours = [...Array(24).keys()];
    this.allMinutes = [...Array(60).keys()];
    this.minDate = this.now.toDate();
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  shouldDisableHour = (hour: number) => (
    this.date.isSameOrBefore(this.now, 'day') && hour < this.now.hours()
  )

  shouldDisableMinute = (minute: number) => (
    this.date.isSameOrBefore(this.now, 'days')
      && this.date.isSameOrBefore(this.now, 'hour')
      && minute < this.now.minutes()
  )

  onDateChange = (event: MatDatepickerInputEvent<Date>) => this.routeFormService.setDate(event.value);
  onHoursChange = (event: MatSelectChange) => this.routeFormService.setHours(event.value);
  onMinutesChange = (event: MatSelectChange) => this.routeFormService.setMinutes(event.value);
}
