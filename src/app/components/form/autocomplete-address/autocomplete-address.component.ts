import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { RouteFormService } from '../../../services/route-form.service';
import { Address, AddressResponse } from '../../../models/addresses';
import { ADDRESS_URL } from '../../../config/api';
import RouteForm from '../../../models/routeForm';

export enum AUTOCOMPLETE_ID {
  DEPARTURE = 'departure',
  ARRIVAL = 'arrival',
}

@Component({
  selector: 'app-autocomplete-address',
  templateUrl: './autocomplete-address.component.html',
  styleUrls: ['./autocomplete-address.component.scss']
})
export class AutocompleteAddressComponent implements OnInit, OnDestroy {

  @Input() id: AUTOCOMPLETE_ID;
  @Input() label: string;
  routeFormSubscription: Subscription;
  formControl = new FormControl();
  addresses = Array<Address>();
  results: string[];

  constructor(private http: HttpClient, private routeFormService: RouteFormService) {}

  ngOnInit() {
    this.routeFormSubscription = this.routeFormService.routeFormSubject.subscribe(
      (route: RouteForm) => {
        if (this.id === AUTOCOMPLETE_ID.DEPARTURE) {
          if (route.departure && route.departure.properties.label !== this.formControl.value) {
            this.formControl.setValue(route.departure.properties.label);
          } else if (!route.departure) {
            this.formControl.setValue('');
          }
        }
        if (this.id === AUTOCOMPLETE_ID.ARRIVAL) {
          if (route.arrival && route.arrival.properties.label !== this.formControl.value) {
            this.formControl.setValue(route.arrival.properties.label);
          } else if (!route.arrival) {
            this.formControl.setValue('');
          }
        }
      },
      error => console.error(`An error occurred on route subscription : ${error}`),
    );

    this.routeFormService.emitRouteForm();

    this.formControl.valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => value.trim().length > 0 ? this.fetchAddresses(value) : of([]))
      ).subscribe((response: AddressResponse) => {
        if ('features' in response) {
          this.addresses = response.features;
          this.results = response.features.map(f => f.properties.label);
        } else {
          this.results = [];
        }
      });
  }

  ngOnDestroy(): void {
    this.routeFormSubscription.unsubscribe();
  }

  private fetchAddresses = (value: string) => {
    const query = encodeURIComponent(value);
    return this.http.get(`${ADDRESS_URL}/?q=${query}&type=housenumber&autocomplete=1`);
  }

  onOptionSelected = (event: MatAutocompleteSelectedEvent) => {
    const fullAddress = this.addresses.find(address => address.properties.label === event.option.value);

    if (fullAddress) {
      this.id === AUTOCOMPLETE_ID.DEPARTURE
        ? this.routeFormService.setDeparture(fullAddress)
        : this.routeFormService.setArrival(fullAddress);
    }
  }

  shouldDisplayClearButton = () => this.formControl.value.trim().length > 0;

  onClear = () => {
    this.formControl.setValue('');
    this.id === AUTOCOMPLETE_ID.DEPARTURE
      ? this.routeFormService.setDeparture(null)
      : this.routeFormService.setArrival(null);
  }
}
