import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { RouteFormService } from '../../services/route-form.service';
import RouteForm from '../../models/routeForm';
import { Subscription } from 'rxjs';
import { MarkerService } from '../../services/marker.service';
import { MAP_DEFAULT_CENTER, MAP_LAYER } from '../../config/map';
import { RouteService } from '../../services/route.service';
import { Route } from '../../models/route';
import { reverseLngLat } from '../../helpers/MapHelper';
import { PolylineService } from '../../services/polyline.service';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy, AfterViewInit {
  private map: L.Map;
  routeFormSubscription: Subscription;
  routeSubscription: Subscription;

  constructor(
    private routeFormService: RouteFormService,
    private routeService: RouteService,
    private markerService: MarkerService,
    private polylineService: PolylineService
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.initMap();

    this.routeFormSubscription = this.routeFormService.routeFormSubject.subscribe(
      (routeForm: RouteForm) => this.handleMarkers(routeForm),
      error => console.error(`An error occurred on routeForm subscription : ${error}`),
    );
    this.routeFormService.emitRouteForm();

    this.routeSubscription = this.routeService.routeSubject.subscribe(
      (route: Route) => { this.setPolyline(route); },
      error => console.error(`An error occurred on route subscription : ${error}`),
    );
    this.routeService.emitRoute();
  }

  ngOnDestroy(): void {
    this.routeFormSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

  private initMap = (): void => {
    this.map = L.map('map', {
      center: MAP_DEFAULT_CENTER,
      zoom: 12,
    });

    const tiles = L.tileLayer(MAP_LAYER, {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' +
        '&copy; <a href="https://carto.com/attributions">CARTO</a>'
    });
    tiles.addTo(this.map);

    this.markerService.setMap(this.map);
    this.polylineService.setMap(this.map);
  }

  private handleMarkers = (routeForm: RouteForm): void => {
    routeForm.departure
      ? this.markerService.setDepartureMarker(routeForm.departure.geometry.coordinates)
      : this.markerService.clearDepartureMarker();
    routeForm.arrival
      ? this.markerService.setArrivalMarker(routeForm.arrival.geometry.coordinates)
      : this.markerService.clearArrivalMarker();
  }

  private setPolyline = (route: Route): void => {
    if (Object.keys(route).length > 0) {
      this.polylineService.setRoutePath(route.points.coordinates);
    }
  }
}
