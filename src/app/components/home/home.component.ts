import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouteService } from '../../services/route.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  private displayRoadmap: boolean;

  constructor(private routeService: RouteService) { }

  ngOnInit(): void {
    this.routeSubscription = this.routeService.displaySubject.subscribe(
      (displayRoadmap: boolean) => this.displayRoadmap = displayRoadmap,
      error => console.error(`An error occurred on route subscription : ${error}`),
    );
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }
}
