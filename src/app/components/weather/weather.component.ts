import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouteWeather, WeatherService } from '../../services/weather.service';
import { RouteService } from '../../services/route.service';
import { RouteFormService } from '../../services/route-form.service';
import { Subscription } from 'rxjs';
import RouteForm from '../../models/routeForm';
import * as Feather from 'feather-icons';
import { WEATHER_IMAGE_URL } from '../../config/api';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  private routeFormSubscription: Subscription;
  private weatherSubscription: Subscription;
  private routeTime: number;
  private routeForm: RouteForm;
  private weather: RouteWeather;

  constructor(
    private weatherService: WeatherService,
    private routeService: RouteService,
    private routeFormService: RouteFormService
  ) { }

  ngOnInit(): void {
    this.routeSubscription = this.routeService.routeSubject.subscribe(
      (route) => {
        if (route && route.time) {
          this.routeTime = route.time;
          this.getWeather();
        }
      },
      error => console.error(`An error occurred on route subscription : ${error}`)
    );
    this.routeService.emitRoute();

    this.routeFormSubscription = this.routeFormService.routeFormSubject.subscribe(
      (routeForm) => {
        if (routeForm) {
          this.routeForm = routeForm;
          this.getWeather();
        }
      },
      error => console.error(`An error occurred on route form subscription : ${error}`)
    );
    this.routeFormService.emitRouteForm();

    this.weatherSubscription = this.weatherService.weatherSubject.subscribe(
      (weather) => {
        this.weather = weather;
      },
      error => console.error(`An error occurred on weather subscription : ${error}`)
    );

    Feather.replace();
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
    this.routeFormSubscription.unsubscribe();
  }

  private getWeather = () => {
    if (this.routeTime && this.routeForm && !this.weather) {
      this.weatherService.getRouteWeather(this.routeForm, this.routeTime);
    }
  }
}
