import { Component, Input, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import { Weather } from '../../../models/weather';
import { WEATHER_IMAGE_URL } from '../../../config/api';

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss']
})
export class WeatherItemComponent implements OnInit {

  @Input() weather: Weather;

  constructor() {
  }

  ngOnInit() {
    Feather.replace();
  }

  private getWeatherImage = (): string => (
    `${WEATHER_IMAGE_URL}/${this.weather.weather[0].icon}@2x.png`
  )

  private isDay = (): boolean => (
    this.weather.dt > this.weather.sys.sunrise
    && this.weather.dt < this.weather.sys.sunset
  )
}
