import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MapComponent } from './components/map/map.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouteFormComponent } from './components/form/route-form/route-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MAT_DATE_LOCALE,
  MatAutocompleteModule, MatButtonModule,
  MatDatepickerModule, MatIconModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule
} from '@angular/material';
import { DateTimeComponent } from './components/form/date-time/date-time.component';
import { AutocompleteAddressComponent } from './components/form/autocomplete-address/autocomplete-address.component';
import { HttpClientModule } from '@angular/common/http';
import { RouteFormService } from './services/route-form.service';
import { MarkerService } from './services/marker.service';
import { RouteService } from './services/route.service';
import { PolylineService } from './services/polyline.service';
import { RoadmapComponent } from './components/roadmap/roadmap.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { WeatherComponent } from './components/weather/weather.component';
import { WeatherService } from './services/weather.service';
import { WeatherItemComponent } from './components/weather/weather-item/weather-item.component';

registerLocaleData(localeFr, 'fr-FR');

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MapComponent,
    RouteFormComponent,
    DateTimeComponent,
    AutocompleteAddressComponent,
    RoadmapComponent,
    WeatherComponent,
    WeatherItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    RouteFormService,
    RouteService,
    MarkerService,
    PolylineService,
    WeatherService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
