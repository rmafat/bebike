import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { reverseLngLat } from '../helpers/MapHelper';
import { Coordinates } from '../models/route';

@Injectable({
  providedIn: 'root'
})
export class PolylineService {

  private map: L.Map;
  private routePath: L.Polyline;
  private readonly routePathColor = '#4CAF50';

  constructor() { }

  setMap = (map: L.Map) => this.map = map;

  setRoutePath = (coordinates: Coordinates[]) => {
    const formattedCoords = coordinates.map(coordinate => reverseLngLat(coordinate));

    if (!this.routePath) {
      this.routePath = L.polyline(formattedCoords, { color: this.routePathColor })
        .addTo(this.map);
    } else {
      this.routePath.setLatLngs(formattedCoords).addTo(this.map);
    }

    this.map.fitBounds(this.routePath.getBounds(), {
      padding: [5, 5],
    });
  }

  clearRoutePath = () => this.routePath ? this.routePath.setLatLngs([]) : null;
}
