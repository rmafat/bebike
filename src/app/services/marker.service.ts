import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { reverseLngLat } from '../helpers/MapHelper';
import { PolylineService } from './polyline.service';

export enum MarkerColor {
  Green = 'pin-green.svg',
  Red = 'pin-red.svg',
}

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  private map: L.Map;
  private departureMarker: L.Marker;
  private arrivalMarker: L.Marker;

  constructor(private polylineService: PolylineService) { }

  setMap = (map: L.Map) => this.map = map;

  setDepartureMarker = (coordinates: number[]) => {
    if (!this.coordinatesHasChanged(this.departureMarker, coordinates)) {
      return;
    }

    if (!this.departureMarker) {
      this.departureMarker = this.setMarker(coordinates, MarkerColor.Green);
    } else {
      this.departureMarker.setLatLng(reverseLngLat(coordinates)).addTo(this.map);
    }

    this.polylineService.clearRoutePath();
    this.fitBounds();
  }

  clearDepartureMarker = () => {
    if (this.departureMarker) {
      this.departureMarker.remove();
      this.fitBounds();
    }
  }

  setArrivalMarker = (coordinates: number[]) => {
    if (!this.coordinatesHasChanged(this.arrivalMarker, coordinates)) {
      return;
    }

    if (!this.arrivalMarker) {
      this.arrivalMarker = this.setMarker(coordinates, MarkerColor.Red);
    } else {
      this.arrivalMarker.setLatLng(reverseLngLat(coordinates)).addTo(this.map);
    }

    this.polylineService.clearRoutePath();
    this.fitBounds();
  }

  clearArrivalMarker = () => {
    if (this.arrivalMarker) {
      this.arrivalMarker.remove();
      this.fitBounds();
    }
  }

  private coordinatesHasChanged = (marker: L.Marker, coordinates: number[]) => {
    if (!marker) {
      return true;
    }

    const previous = marker.getLatLng();
    return previous.lng !== coordinates[0] || previous.lat !== coordinates[1];
  }

  private fitBounds = () => {
    const bounds = [];
    if (this.departureMarker) {
      bounds.push(this.departureMarker.getLatLng());
    }
    if (this.arrivalMarker) {
      bounds.push(this.arrivalMarker.getLatLng());
    }

    this.map.fitBounds(bounds, {
      maxZoom: 16,
      padding: [10, 10],
    });
  }

  private setMarker = (coordinates: number[], color: MarkerColor) => (
    L.marker(
      reverseLngLat(coordinates),
      {
        icon: this.getIcon(color),
      },
    ).addTo(this.map)
  )

  private getIcon = (color: string): L.Icon => {
    return L.icon({
      iconUrl: `assets/${color}`,
      iconSize: [35, 35],
      iconAnchor: [17, 36],
    });
  }
}
