import { Injectable } from '@angular/core';
import { Route } from '../models/route';
import { Subject } from 'rxjs';
import RouteForm from '../models/routeForm';
import { ROUTE_URL } from '../config/api';
import { MODES } from '../models/modes';
import { ROUTE_FREE_KEY, ROUTE_KEY } from '../../parameters';
import { fetchData } from '../helpers/ApiHelper';

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  route: Route;
  displayRoadmap = false;
  routeSubject = new Subject<Route>();
  displaySubject = new Subject<boolean>();

  constructor() {
    this.route = new Route();
  }

  emitRoute = () => this.routeSubject.next(this.route);
  emitDisplay = () => this.displaySubject.next(this.displayRoadmap);

  fetchRoute = async (routeForm: RouteForm): Promise<void> => {
    if (!routeForm.departure || !routeForm.arrival) {
      console.warn('You can\'t search route without departure and arrival points.');
      return;
    }

    let url = ROUTE_URL;
    url += `?point=${routeForm.departure.geometry.coordinates[1]},${routeForm.departure.geometry.coordinates[0]}`;
    url += `&point=${routeForm.arrival.geometry.coordinates[1]},${routeForm.arrival.geometry.coordinates[0]}`;
    url += `&vehicle=${routeForm.mode === MODES.PEDESTRIAN ? 'foot' : 'bike'}`;
    url += `&type=json`;
    url += `&locale=fr-FR`;
    url += `&points_encoded=false`;
    url += `&weighting=fastest`;
    url += `&elevation=true`;
    url += `&key=${ROUTE_FREE_KEY}`;

    const route = await fetchData(url);

    this.route = route.paths[0];
    this.displayRoadmap = true;
    this.emitRoute();
    this.emitDisplay();
  }

  hideResults = () => {
    this.displayRoadmap = false;
    this.emitDisplay();
  }
}
