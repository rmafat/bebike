import { Injectable } from '@angular/core';
import { ListWeather, Weather } from '../models/weather';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { Moment } from 'moment';
import RouteForm from '../models/routeForm';
import { fetchData } from '../helpers/ApiHelper';
import { WEATHER_CURRENT_URL, WEATHER_FORECAST_URL } from '../config/api';
import { WEATHER_KEY } from '../../parameters';

export interface RouteWeather {
  departure: Weather;
  arrival: Weather;
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  weather: RouteWeather;
  weatherSubject = new Subject<RouteWeather>();

  constructor() {
    this.weather = {
      departure: null,
      arrival: null,
    };
  }

  emitWeather = () => this.weatherSubject.next(this.weather);

  getRouteWeather = async (routeForm: RouteForm, routeTime: number) => {
    const departureDate = moment(routeForm.date);
    const arrivalDate = departureDate.add(routeTime, 'ms');

    this.weather.departure = await this.getWeather(departureDate, routeForm.departure.geometry.coordinates);
    this.weather.arrival = await this.getWeather(arrivalDate, routeForm.arrival.geometry.coordinates);

    return this.emitWeather();
  }

  getWeather = async (date: Moment, coordinates: number[]): Promise<Weather> => {
    const now = moment();

    // No weather data after 5 days.
    if (date.diff(now, 'hours') >= (24 * 5)) {
      return null;
    }

    // Less than 2 hours -> Get current weather.
    if (date.diff(now, 'hours') < 2) {
      return await this.fetchCurrentWeather(coordinates);
    }

    // Get forecast list (3h/5d).
    const forecast = await this.fetchForecastWeather(coordinates);
    return this.getClosestWeather(date, forecast);
  }

  fetchCurrentWeather = async (coordinates: number[]): Promise<Weather> => {
    let url = `${WEATHER_CURRENT_URL}${this.getBaseParams()}`;
    url = `${url}&lat=${coordinates[0]}&lon=${coordinates[1]}`;

    return await fetchData(url);
  }

  fetchForecastWeather = async (coordinates: number[]): Promise<ListWeather> => {
    let url = `${WEATHER_FORECAST_URL}${this.getBaseParams()}`;
    url = `${url}&lat=${coordinates[0]}&lon=${coordinates[1]}`;

    return await fetchData(url);
  }

  private getBaseParams = (): string => (
    `?appid=${WEATHER_KEY}&units=metric&lang=fr`
  )

  private getClosestWeather = (date: Moment, forecast: ListWeather) => {
    // = 1h30 to seconds.
    const interval = 5400;
    const unixDate = date.unix();

    const closestWeather = forecast.list.find((weather) => (
      Math.abs(unixDate - weather.dt) < interval
    ));

    if (closestWeather === undefined) {
      return null;
    }

    closestWeather.sys.sunrise = forecast.city.sunrise;
    closestWeather.sys.sunset = forecast.city.sunset;

    return closestWeather;
  }
}
