import { Injectable } from '@angular/core';
import * as moment from 'moment';
import RouteForm from '../models/routeForm';
import { Subject } from 'rxjs';
import { Address } from '../models/addresses';
import { MODES } from '../models/modes';
import { Moment } from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RouteFormService {

  routeForm: RouteForm;
  routeFormSubject = new Subject<RouteForm>();

  constructor() {
    this.routeForm = new RouteForm();
  }

  emitRouteForm = () => {
    this.routeFormSubject.next(this.routeForm);
  }

  setDeparture = (departure: Address) => {
    this.routeForm.departure = departure;
    this.emitRouteForm();
  }

  setArrival = (arrival: Address) => {
    this.routeForm.arrival = arrival;
    this.emitRouteForm();
  }

  switchDirections = () => {
    const { departure } = this.routeForm;
    this.routeForm.departure = this.routeForm.arrival;
    this.routeForm.arrival = departure;
    this.emitRouteForm();
  }

  setMode = (mode: MODES) => {
    this.routeForm.mode = mode;
    this.emitRouteForm();
  }

  setDate = (date: Moment|Date) => {
    if (date instanceof Date) {
      date = moment(date);
    }

    this.routeForm.newDate = date;
    this.emitRouteForm();
  }

  setHours = (hours: number) => {
    this.routeForm.hours = hours;
    this.emitRouteForm();
  }
  setMinutes = (minutes: number) => {
    this.routeForm.minutes = minutes;
    this.emitRouteForm();
  }
}
