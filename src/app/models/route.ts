export type Coordinates = [number, number] | [number, number, number];

export interface Polyline {
  type: string;
  coordinates: Coordinates[];
}

export enum InstructionSign {
  KEEP_LEFT = -7,
  SHARP_LEFT = -3,
  LEFT = -2,
  SLIGHT_LEFT  = -1,
  STRAIGHT = 0,
  SLIGHT_RIGHT = 1,
  RIGHT = 2,
  SHARP_RIGHT = 3,
  FINISH = 4,
  REACHED = 5,
  ROUNDABOUT = 6,
  KEEP_RIGHT = 7,
}

export interface Instruction {
  distance: number;
  heading?: number;
  sign: InstructionSign;
  interval: [number, number];
  text: string;
  time: number;
  street_name: string;
  annotation_text?: string;
  annotation_importance?: number;
  exit_number?: number;
  turn_angle?: number;
  exited?: boolean;
  last_heading?: number;
}

export class Route {
  distance: number;
  time: number;
  points: Polyline;
  instructions: Instruction[];
  ascend: number;
  descend: number;

  constructor() {}
}
