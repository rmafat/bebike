interface WeatherDescription {
  id: string;
  main: string;
  description: string;
  icon: string;
}

interface Temperature {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

interface Wind {
  speed: number;
  deg: number;
}

interface WeatherSystem {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
}

export interface ListWeather {
  list: Weather[];
  city: {
    sunrise: number;
    sunset: number;
  };
}

export class Weather {
  coord?: {
    long: number,
    lat: number,
  };
  weather: WeatherDescription[];
  main: Temperature;
  wind: Wind;
  clouds: {
    all: number;
  };
  dt: number;
  sys: WeatherSystem;
  id?: number;
  name?: string;

  constructor() {}
}
