import * as moment from 'moment';
import { Moment } from 'moment';
import { Address } from './addresses';
import { MODES } from './modes';

export default class RouteForm {
  departure: Address;
  arrival: Address;
  date: Moment;
  mode: MODES;

  constructor() {
    this.date = moment();
    this.mode = MODES.BIKE;
  }

  set newDate(newDate: Moment) {
    const hours = this.date.hours();
    const minutes = this.date.minutes();

    newDate.hours(hours);
    newDate.minutes(minutes);

    this.date = newDate;
  }

  set hours(hour: number) {
    this.date.hours(hour);
  }

  set minutes(minutes: number) {
    this.date.minutes(minutes);
  }
}
