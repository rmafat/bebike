export interface AddressResponse {
  features: Address[];
  query: string;
  limit: number;
}

export interface Address {
  geometry: {
    coordinates: [number, number],
  };
  properties: {
    label: string,
    score: number,
    id: string,
    type: string,
    x: number,
    y: number,
    importance: number,
    name: string,
    postcode: string,
    citycode: string,
    city: string,
    context: string,
  };
}
