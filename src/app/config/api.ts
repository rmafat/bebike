export const ADDRESS_URL = 'https://api-adresse.data.gouv.fr/search';
export const ROUTE_URL = 'https://graphhopper.com/api/1/route';

export const WEATHER_CURRENT_URL = 'https://api.openweathermap.org/data/2.5/weather';
export const WEATHER_FORECAST_URL = 'https://api.openweathermap.org/data/2.5/forecast';
export const WEATHER_IMAGE_URL = 'https://openweathermap.org/img/wn';
