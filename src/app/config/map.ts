export const MAP_LAYER = 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png';
export const MAP_DEFAULT_CENTER: [number, number] = [44.837789, -0.57918];
